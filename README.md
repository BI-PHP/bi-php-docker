# Docker images for BI-PHP
In this repository, you'll find the Dockerfiles used for creating Docker images for the BI-PHP course at FIT CTU in Prague. 

Directories and files:
* `build`: Dockerfiles themselves. Unless you are interested in details of how this all works, you do not need to read them.
* `docker-compose.yaml`: A minimum example of directives for Docker Compose
* `src`: Put your PHP source **here**.

Instructions for usage can be found at [Course pages for BI-PHP](https://courses.fit.cvut.cz/BI-PHP/configuration/docker-config.html).